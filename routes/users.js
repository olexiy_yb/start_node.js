var express = require('express');
var router = express.Router();

const { updUser } = require("../services/user.service");
const { delUser } = require("../services/user.service");
const { addUser } = require("../services/user.service");
const { getUser } = require("../services/user.service");
const { getUsers } = require("../services/user.service");
const { isAuthorized } = require("../middlewares/auth.middleware");

router.get('/', function(req, res, next) {
  const users = getUsers();
  if (users) {
    res.send(users);
  } else {
    res.status(400).send(`Some error`);
  }
});

router.get('/:id', function(req, res, next) {
  const user = getUser(req.params.id);
  if (user) {
    res.send(user);
  } else {
    res.status(400).send(`Some error`);
  }
});

router.post('/'/* , isAuthorized */, function(req, res, next) {
  const result = addUser(req.body);
  
  if (result) {
    res.send(`adding done`);
  } else {
    res.status(400).send(`Some error`);
  }
});

router.delete('/:id'/* , isAuthorized */, function(req, res, next) {
  const result = delUser(req.params.id)
  if (result) {
    res.send(`delete done`);
  } else {
    res.status(400).send(`Some error`);
  }
});

router.put('/:id'/* , isAuthorized */, function(req, res, next) {
  const result = updUser(req.params.id, req.body);
  console.log(result);
  if (result) {
    res.send(`put done`);
  } else {
    res.status(400).send(`Some error`);
  }
})

module.exports = router;
