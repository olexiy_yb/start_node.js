const { updData } = require("../repositories/user.repository");
const { insData } = require("../repositories/user.repository");
const { searchData } = require("../repositories/user.repository");
const { deleteData } = require("../repositories/user.repository");
const { getDataId } = require("../repositories/user.repository")
const { getData } = require("../repositories/user.repository");

const saveName = (user) => {
  if (user) {
    return saveData(user.name);
  } else {
    return null;
  }
};

const getUsers = () => {
  return getData();
}

const getUser = (id) => {
  return getDataId(id);
}

const addUser = (user) => {  
  if (user) {
    return insData(user);
  } else {
    return null;
  }
}

const delUser = (id) => {
  const ind = searchData(id);
  if (ind>=0) {
    return deleteData(ind);
  }
  return false;
}

const updUser = (id, user) => {
  const ind = searchData(id);
  return updData(ind, user);
  
}


module.exports = {
  saveName,
  getUsers,
  getUser,
  addUser,
  delUser,
  updUser,
};